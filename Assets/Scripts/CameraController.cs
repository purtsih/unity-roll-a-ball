﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    public float speed = 0.5f;

    private Vector3 offset;
    private Vector3 velocity;

	// Use this for initialization
	void Start () {
		offset = transform.position - player.transform.position;
	}
	
	void LateUpdate () {
        // Smoothly move towards real camera position
        // For this to work, player rigidbody has to have Interpolate on
        // https://docs.unity3d.com/ScriptReference/Rigidbody-interpolation.html
        // https://docs.unity3d.com/ScriptReference/Vector3.SmoothDamp.html
        transform.position = Vector3.SmoothDamp(
            transform.position,
            player.transform.position + offset,
            ref velocity,
            speed
        );
	}
}
