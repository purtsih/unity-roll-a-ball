﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
    void Start ()
    {
        // Add a random offset to rotation
        // This makes affected object have a unique starting rotation
        transform.Rotate(new Vector3(
            Random.Range(-50, 50),
            Random.Range(-50, 50),
            Random.Range(-50, 50)
        ));
    }

    void Update ()
    {
        transform.Rotate((new Vector3(15, 30, 45)) * Time.deltaTime);
    }
}
