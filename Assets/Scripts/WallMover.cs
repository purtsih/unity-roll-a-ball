﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMover : MonoBehaviour {
    private Vector3 start;

    public Transform end;
    public float duration;

    void Awake ()
    {
        // Rotate wall to face end point
        transform.rotation = Quaternion.LookRotation(end.position - transform.position);

        // Save this transform as the starting position
        start = transform.position;
    }

    void Update ()
    {
        // Move wall back and forth
        // https://docs.unity3d.com/ScriptReference/Mathf.PingPong.html

        float rate = Mathf.PingPong(Time.time, duration) / duration;
        transform.position = Vector3.Lerp(
            start,
            end.position,
            rate
        );
    }
}
